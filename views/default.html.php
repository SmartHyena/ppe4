<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">

<html lang="fr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title> PPE4 </title>
        <meta name="generator" content="TextMate http://macromates.com/">
        <meta name="author" content="Fabrice Luraine">
        <!-- Date: 2009-06-25 -->
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="views/assets/knacss.css">

    </head>
    <body>
        <h1> Limonade test with knacss </h1>
        <div id="content">
            <?php echo $content; ?>
        </div>
        <hr>

        <p id="nav">
        </p>

    </body>
</html>
