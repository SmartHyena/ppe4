<?php 

    /*
        CRUD USER
    */

    class User{

        private $credentials = '';
        private $db = null;

        public function setCredentials($credentials){
            $this->credentials = $credentials;
            $this->connect();
        }

        private function connect(){
            $this->db = new Db();
            $this->db->connect($this->credentials);
        }
            
        public function disconnect(){
            $this->db->disconnect();
            $this->db = null;
        }

        public function create($login, $noHashPassword, $name, $lastName, $phoneNumber, $email){

            // check if login already used
            if(count($this->read($login)) == 1){
                return array("result"=>false, "msg"=>"Login already used");
            }else{
                try{

                    $stmt = $this->db->getPdo()->prepare("INSERT INTO users (login, password, name, last_name, phone_number, email) 
                    VALUES (:login ,:password, :name, :last_name, :phone_number, :email)");

                    $hashedPassword = crypt_password($noHashPassword);

                    $stmt->bindParam(':login', $login);
                    $stmt->bindParam(':password', $hashedPassword);
                    $stmt->bindParam(':name', $name);
                    $stmt->bindParam(':last_name', $lastName);
                    $stmt->bindParam(':phone_number', $phoneNumber);
                    $stmt->bindParam(':email', $email);

                    $stmt->execute();

                    return array("result"=>true, "msg"=>"");

                }catch(PDOException $e) {
                    $msg = 'ERREUR PDO dans ' . $e->getFile() . ' L.' . $e->getLine() . ' : ' . $e->getMessage(); 
                    return array("result"=>false, "msg"=>$msg);
                    echo($msg);
                    die($msg);
                }
            }
        }

        public function read($login){

            $stmt = $this->db->getPdo()->prepare("SELECT users.login, users.password, users.name, users.last_name, users.phone_number, users.email FROM users WHERE users.login = :login");
            $stmt->bindParam(':login', $login);
            
            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_ASSOC); 

            return $stmt->fetchAll();
            
        }

        public function update($login, $newData, $updatePassword){

            // check if login exist
            if(count($this->read($login)) == 1){
                try{
                    $stmt = null;
                    if($updatePassword){

                        $stmt = $this->db->getPdo()->prepare("UPDATE users SET users.password=:password WHERE login=:login");

                        $stmt->bindParam(':login', $login);
                        $stmt->bindParam(':password', $hashedPassword);
                        $hashedPassword = crypt_password($newData["password"]);
                        
                    }else{
                        $stmt = $this->db->getPdo()->prepare("UPDATE users SET users.name=:name, users.last_name=:last_name, users.phone_number=:phone_number, users.email=:email WHERE login=:login");

                        $stmt->bindParam(':login', $login);
                        $stmt->bindParam(':name', $newData["name"]);
                        $stmt->bindParam(':last_name', $newData["last_name"]);
                        $stmt->bindParam(':phone_number', $newData["phone_number"]);
                        $stmt->bindParam(':email', $newData["email"]);
                    }
                        
                    $stmt->execute();

                    return array("result"=>true, "msg"=>"");

                }catch(PDOException $e) {
                    $msg = 'ERREUR PDO dans ' . $e->getFile() . ' L.' . $e->getLine() . ' : ' . $e->getMessage(); 
                    return array("result"=>false, "msg"=>$msg);
                    echo($msg);
                    die($msg);
                }
            }else{
                return array("result"=>false, "msg"=>"Login does not exist");
            }
        }

        public function delete($login){

            // check if login exist
            if(count($this->read($login)) == 1){
                try{
                    
                    $stmt = $this->db->getPdo()->prepare("DELETE FROM users WHERE login=:login");

                    $stmt->bindParam(':login', $login);
                    $stmt->execute();

                    return array("result"=>true, "msg"=>"t");

                }catch(PDOException $e) {
                    $msg = 'ERREUR PDO dans ' . $e->getFile() . ' L.' . $e->getLine() . ' : ' . $e->getMessage(); 
                    return array("result"=>false, "msg"=>$msg);
                    echo($msg);
                    die($msg);
                }
                    

            }else{
                return array("result"=>false, "msg"=>"Login does not exist");
            }

        }

    }

?>


