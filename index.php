<?php

# 0. Loading limonade framework
require_once('lib/limonade/limonade.php');

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

# 1. Setting global options of our application
function configure(){

  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);

  # A. Setting environment
  $localhost = preg_match('/^localhost(\:\d+)?/', $_SERVER['HTTP_HOST']);
  $env =  $localhost ? ENV_DEVELOPMENT : ENV_PRODUCTION;
  option('env', $env);
  option('pages_dir', file_path(option('root_dir'), 'views'));
  
  # B. Initiate db connexion
  /*
	$dsn = $env == ENV_PRODUCTION ? 'sqlite:db/prod.db' : 'sqlite:db/dev.db';
	try
	{
	  $db = new PDO($dsn);
	}
	catch(PDOException $e)
	{
	  halt("Connexion failed: ".$e); # raises an error / renders the error page and exit.
	}
	$db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
	option('db_conn', $db);*/
	
	# C. Other options
	setlocale(LC_TIME, "fr_FR");
}


# 2. Setting code that will be executed bfore each controller function
function before(){
  layout('default.html.php');
}

# 3. Defining routes and controllers
# ----------------------------------------------------------------------------
# RESTFul map:
#
#  HTTP Method |  Url path         |  Controller function
# -------------+-------------------+-------------------------------------------
#   GET        |  /posts           |  blog_posts_index
#   GET        |  /posts/:id       |  blog_posts_show 
#   GET        |  /posts/new       |  blog_posts_new 
#   POST       |  /posts           |  blog_posts_create
#   GET        |  /posts/:id/edit  |  blog_posts_edit 
#   PUT        |  /posts/:id       |  blog_posts_update
#   DELETE     |  /posts/:id       |  blog_posts_destroy
#   GET        |  /                |  blog_posts_home (redirect to /posts)
# -------------+-------------------+-------------------------------------------
#

function load_middlewares(){

    require_once('controlers/crypt.php');
    require_once('controlers/auth.php');
    require_once('controlers/db.php');
    require_once('controlers/workflow.php');

}

load_middlewares();

# matches GET /
dispatch('/', 'hello_world');
function hello_world(){
    // redirect_to('posts'); # redirects to the index
    // set('posts', $posts); # passing posts to the view

    
    return html('hello.html.php'); # rendering HTML view
    
}

# matches GET /posts  
dispatch('/posts', 'blog_posts_index');
function blog_posts_index(){
  
  return html('hello.html.php'); # rendering HTML view
}

dispatch('/crypt', 'do_crypt');
function do_crypt(){

    // return html('hello.html.php'); # rendering HTML view
    echo('root: ');
    echo(crypt_password('root'));

    return html('hello.html.php'); # rendering HTML view
    
}

dispatch('/read', 'readTest');
function readTest(){

    require_once('model/user.php');
    require_once('credentials.php');

    $user = new User();
    $user->setCredentials($credentials);

    $rows = $user->read('philippe_admin');
    $user->disconnect();

    var_dump($rows);


    return html('hello.html.php'); # rendering HTML view
    
}

dispatch('/create', 'createTest');
function createTest(){

    require_once('model/user.php');
    require_once('credentials.php');

    $user = new User();
    $user->setCredentials($credentials);
    
    $result = $user->create('philippe_admin', 'root', 'rooti', 'rooto', 036242045, 'skejbskebg');
    var_dump($result);

    $result2 = $user->create('philippe', 'root', 'rooti', 'rooto', 036242045, 'skejbskebg');
    var_dump($result2);

    $user->disconnect();

    return html('hello.html.php'); # rendering HTML view
    
}

dispatch('/decrypt', 'decryptTest');
function decryptTest(){

    require_once('model/user.php');
    require_once('credentials.php');

    $crud_user = new User();
    $crud_user->setCredentials($credentials);
    
    $user = $crud_user->read('philippe_admin')[0];
    $crud_user->disconnect();

    var_dump($user);

    if(check_passwords('root', $user["password"])){
      echo("password matches");
    }else{
      echo("password dont matches");
    }

    if(check_passwords('drgdrgdr', $user["password"])){
      echo("password matches");
    }else{
      echo("password dont matches");
    }

    return html('hello.html.php'); # rendering HTML view
    
}

dispatch('/update', 'updateTest');
function updateTest(){

    require_once('model/user.php');
    require_once('credentials.php');

    $crud_user = new User();
    $crud_user->setCredentials($credentials);
    
    // $crud_user->update('philippedf', array("password"=>"sdfsdf"), true);
    // $crud_user->update('philippe', array("password"=>"rootroot"), true);
    $result = $crud_user->update('philippe', array("name"=>"NouveauNom", "last_name"=>"bojolai", "phone_number"=>5646546, "email"=>"srgdrgdgdrgdrg"), false);
    var_dump($result);

    $crud_user->disconnect();

    return html('hello.html.php'); # rendering HTML view
    
}

dispatch('/delete', 'deleteTest');
function deleteTest(){

    require_once('model/user.php');
    require_once('credentials.php');

    $crud_user = new User();
    $crud_user->setCredentials($credentials);
    
    $result = $crud_user->delete('philippedrgdrg');
    var_dump($result);

    $result2 = $crud_user->delete('philippe');
    var_dump($result2);

    $crud_user->disconnect();

    return html('hello.html.php'); # rendering HTML view
    
}

/*
# matches GET /posts  
dispatch('/posts', 'blog_posts_index');
  function blog_posts_index()
  {
    $posts = post_find_all();
    set('posts', $posts); # passing posts to the view
    return html('posts/index.html.php'); # rendering HTML view
  }

# matches GET /posts/new
# must be written before the /posts/:id route
dispatch('/posts/new', 'blog_posts_new');
  function blog_posts_new()
  { 
    # passing an empty post to the view
    set('post', array('id'=>'', 'title'=>'Your title here...', 'body'=>'Your content...'));
    return html('posts/new.html.php'); # rendering view
  }

# matches GET /posts/1  
dispatch('/posts/:id', 'blog_posts_show');
  function blog_posts_show()
  { 
    if( $post = post_find(params('id')) )
    {
      set('post', $post); # passing the post the the view
      return html('posts/show.html.php'); # rendering the view
    }
    else
    {
      halt(NOT_FOUND, "This post doesn't exists"); # raises error / renders an error page
    }
    
  }
  
# matches POST /posts
dispatch_post('/posts', 'blog_posts_create');
  function blog_posts_create()
  { 
    if($post_id = post_create($_POST['post']))
    {
      redirect_to('posts', $post_id); # redirects to the show page of this newly created post
    }
    else
    {
      halt(SERVER_ERROR, "AN error occured while trying to create a new post"); # raises error / renders an error page
    }
  }
  
# matches GET /posts/1/edit  
dispatch('/posts/:id/edit', 'blog_posts_edit');
  function blog_posts_edit()
  {
    if($post = post_find(params('id')))
    {
      set('post', $post); # passing the post the the view
      return html('posts/edit.html.php'); # rendering the edit view, with its form
    }
    else
    {
      halt(NOT_FOUND, "This post doesn't exists. Can't edit it."); # raises error / renders an error page
    }
  }
  
# matches PUT /posts/1
dispatch_put('/posts/:id', 'blog_posts_update');
  function blog_posts_update()
  {
    $post_id = params('id');
    if(post_update($post_id, $_POST['post']))
    {
      redirect_to('posts', $post_id); # redirects to this freshly just updated post
    }
    else
    {
      halt(SERVER_ERROR, "An error occured while trying to update post ".$post_id); # raises error / renders an error page
    }
  }
  
# matches DELETE /posts/1
dispatch_delete('/posts/:id', 'blog_posts_destroy');
  function blog_posts_destroy()
  {
    $post_id = params('id');
    if($post = post_destroy($post_id))
    {
      redirect_to('posts'); # redirects to the index
    }
    else
    {
      halt(SERVER_ERROR, "An error occured while trying to destroy post ".$post_id); # raises error / renders an error page
    }
  }
*/


# 4. Running the limonade blog app
run();

