<?php 

    // +/- 60 caractères
    function crypt_password($user_input){
        $options = [
            'cost' => 12,
        ];
        return password_hash($user_input, PASSWORD_BCRYPT, $options);
    }

    function check_passwords($user_input, $hashed_password){
        if (password_verify($user_input, $hashed_password)) {
            return true;
        } else {
            return false;
        }
    }

?>