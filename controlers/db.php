<?php 

    class Db{
        
        private $pdo;

        public function connect($credentials){
            try {

                $arrExtraParam= array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"); 
                $strConnection = 'mysql:host='. $credentials["host"] .';dbname=' . $credentials["dbname"]; 

                $this->pdo = new PDO($strConnection, $credentials["user"], $credentials["password"], $arrExtraParam); 
                $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                
            } catch(PDOException $e) {
                $msg = 'ERREUR PDO dans ' . $e->getFile() . ' L.' . $e->getLine() . ' : ' . $e->getMessage(); die($msg);
            }
        }

        public function disconnect(){
            $this->pdo = null;
        }

        public function getPdo(){
            return $this->pdo;
        }

    }

?>